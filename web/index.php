<?php // @codingStandardsIgnoreLine
/** 
 * La page principal du programme SeasonManager
 * PHP Version 7.2.2
 * 
 * @category Classe
 * @package  Null
 * @author   Chevallier Michael <mica.2727@hotmail.fr>
 * @license  Creative Commons BY-NC-ND
 * @link     https://www.cc-i.ovh 
 */
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
?>
<html lang="fr">
    <head>
        <link rel="stylesheet" href="lib/bower_components/semantic/dist/semantic.min.css" />
        <link rel="stylesheet" href="css/style.css" />
    </head>
    <body>
        <header>
            <img src="img/logo_seasonManager.png" />
        </header>

        <div class="ui blue pointing inverted stackable center aligned menu">
            <a data-name="home" class="active item">
                <i class="icon home"></i> 
                Accueil
            </a>
            <a data-name="series" class="item">
                <i class="icon video"></i>
                Séries
            </a>
            <a data-name="movies" class="item">
                <i class="icon film"></i>
                Films
            </a>
            <a data-name="mangas" class="item">
                <i class="icon book"></i>
                Mangas/BD
            </a>
            <a data-name="books" class="item">
                <i class="icon book"></i>
                Livres
            </a>
        </div>

        <app-page></app-page>
        
        <div data-id="content" class="ui segment">
            <h2 data-id="title">Qu'est-ce que SeasonManager ?</h2>
            <div class="ui segments">
                <div class="ui segment">
                    <h3 class="ui header top">SeasonManager c'est :</h3>
                </div>
                <div class="ui segment">
                    <ul class="ui list">
                        <li>Un script écrit en langage web et destiné à vous facilitez la vie sur ce que vous regardez à la tv,
                            certains y passe des heures et il devient difficile de savoir ce que vous avez vue ou non.</li>
                        <li>Il permet de...</li>
                        <ul>
                            <li>Gérer les séries que vous regardez pour vous rappeler de celui que vous avez vue.</li>
                            <li>Gérer les films que vous avez déjà vue ainsi que l'année ou vous l'avez vue.</li>
                            <li>Gérer les mangas que vous avez vue (tome, chapitre, etc).</li>
                            <li>Gérer vos livres dans le même principe que les mangas. Ainsi que là ou vous vous êtes arrêter.</li>
                        </ul>
                        <li>Cet outil utilise :</li>
                        <ul>
                            <li><i class="icon php"></i> PHP : <a href="https://php.net">https://php.net</a></li>
                            <li><i class="icon js"></i> jQuery : <a href="https://jquery.com">https://jquery.com</a></li>
                            <li><i class="icon css3"></i> Semantic UI : <a href="https://semantic-ui.com">https://semantic-ui.com</a></li>
                            <li>Font-Awesome : <a href="https://fontawesome.com">https://fontawesome.com</a> (Inclus dans SemanticUI)
                        </ul>
                        <li>Informations complémentaires</li>
                        <ul>
                            <li>L'outil SeasonManager est développer par Chevallier Michael, vous pouvez le télécharger gratuitement mais ne pas le redistribuer.</li>
                            <li>Il n'est pas utilisable sur un serveur web, c'est destiné a une utilisation personnel et comme un logiciel.
                                <br />Cet outil ne possède aucun espace membre et si vous le mettez sur un serveur web tout pourrait être supprimer.
                            </li>
                            <li>Je ne suis pas responsable de comment vous utilisez mon outil dans le principe ou je n'ai aucune information sur ceux qui le télécharge.</li>
                            <li>L'outil ne récupère aucune information sur vous, ni sur son utilisation.</li>
                        </ul>
                    </ul>
                </div>
            </div>
        </div>

        <div class="ui segment">
            <div class="ui center floated mini">
                &copy; <a href="https://www.cc-i.ovh">Creative Community & Innovation</a> 2018 - Tous droit réservés - Version 1.00 (23/03/2017)
            </div>
        </div>
        <script src="lib/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="lib/bower_components/semantic/dist/semantic.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
        <script>
                // var = global
                // let = porté limité au bloc

                var Stockage = {
                    jsonLength: 0,
                    dataEntry: [],
                    dataEntrySize: 0,

                    // Méthodes pour JsonLength
                    IncrementJsonLength() {
                        this.jsonLength++;
                    },
                    DecrementJsonLength() {
                        this.jsonLength--;
                    },
                    ResetJsonLength() {
                        this.jsonLength = 0;
                    },
                    GetJsonLength() {
                        return this.jsonLength;
                    },

                    // Méthodes pour dataEntry
                    InitDataEntry($data) {
                        this.dataEntry = $data;
                        let dSize = 0;
                        $.each(this.dataEntry,() => {dSize++;});
                        this.dataEntrySize = dSize;
                    },
                    AddDataEntry($data) {
                        this.dataEntry[this.dataEntrySize] = $data;
                        return this.dataEntry;
                    },
                    EditDataEntry($id,$data) {
                        let originData = this.dataEntry;
                        SeasonManager._GetTypeFull(SeasonManager.namePage).forEach((el,b) => {
                            originData[$id][el] = $data[el];
                        });
                        originData[$id]['lastedit'] = this._GetDate();
                        // - console.log(this._GetDate());
                        //let date = new Date();
                        //console.log(date.now());
                        this.dataEntry = originData;
                        this.InitDataEntry(this.dataEntry);
                        return this.dataEntry;
                    },
                    RemoveDataEntry($id) {
                        let tmp = new Object();
                        $.each(this.dataEntry, ($key,$val) => {
                            if($key != $id) {
                                tmp[$key] = new Object();
                                $.each($val, ($k,$v) => {
                                    tmp[$key][$k] = $v;
                                });
                            }
                        });

                        this.InitDataEntry(tmp);
                    },
                    GetDataEntry() {
                        return this.dataEntry;
                    },
                    ResetDataEntry() {
                        this.dataEntry = [];
                    },
                    SaveDataEntry(datas) {
                        // - console.log(SeasonManager.namePage);
                        // - console.log(`Save !!! ${SeasonManager.namePage} - ${this.GetDataEntry()}`);
                        $.ajax({
                            url: "actions.php",
                            method: "POST",
                            data: {
                                "page":SeasonManager.namePage,
                                "opt":"saveData",
                                "dataEntry": this.GetDataEntry()
                            }
                        })
                        .done((msg) => {
                            // - console.log(`log: ${msg}`);
                        });
                    },

                    _GetDate() {
                        let time = new Date(Date.now());
                        let jour = time.getDate();
                        let mois = time.getMonth() + 1;
                        let annee = time.getFullYear();
                        let heures = time.getHours();
                        let minutes = time.getMinutes();
                        let secondes = time.getSeconds();

                        return ( (jour<10)     ? '0'+jour      : jour ) + '/' + 
                            ( (mois<10)     ? '0'+mois      : mois ) + '/' + 
                            annee + ' ' + 
                            ( (heures<10)   ? '0'+heures    : heures ) + ':' + 
                            ( (minutes<10)  ? '0'+minutes   : minutes ) + ':' +
                            ( (secondes<10) ? '0'+secondes  : secondes);
                    }
                };

                class Database {
                    constructor($page) {
                        this.filename = "db/" + $page;
                        this.db = "";
                        this.page = $page;
                        this.base = $("app-page");
                        this.sizeJson = 0;

                        // - console.log(this.filename);
                    }

                    GetFileJSON() {
                        if(this.page == "series") {
                            Stockage.ResetDataEntry();
                            Stockage.ResetJsonLength();
                            let tmp = new Object();
                            //let appPage = $("app-page").html();
                            $.getJSON( this.filename, data => {
                                $.each( data, function($key,$val) {
                                    var e = $("app-page").html();
                                    e = e.replace('{{id}}',$key);
                                    tmp[$key] = new Object();
                                    tmp[$key]['id'] = $key;
                                        
                                    // Traitement spécial pour l'urlstream
                                    // Remplace {{name}} par le nom de la série dans {{urlstream}}.
                                    $val['urlstream'] = $val['urlstream'].replace(new RegExp('{{name}}','g'),$val['name']);
                                    // Traitement spécial pour name
                                    // Ajoute la couleur rouge foncé quand la série est défini comme "terminée".
                                    let checkStateTerminee = $val['name'].match(/\(f\)/);
                                    // Ajoute la couleur violet quand la série est défini comme "annulée".
                                    let checkStateAnnulee = $val['name'].match(/\(c\)/);
                                    // Ajoute la couleur verte quand la série est défini comme "prod" et supprime le message "prod".
                                    let checkStateProd = $val['name'].match(/\(p\)/);

                                    if(checkStateTerminee != null) {
                                        // -console.log("test");
                                        // -console.log($val.name);
                                        let val = $val;
                                        $val['name'] = `<span style='color: darkred;'>${$val['name']}</span>`;
                                        $val['name'] = $val['name'].replace(/\(f\)/,'');
                                    } else if(checkStateAnnulee != null) {
                                        $val['name'] = `<span style='color:red;'>${$val['name']}</span>`;
                                        $val['name'] = $val['name'].replace(/\(c\)/,'');
                                    } else if(checkStateProd != null) {
                                        $val['name'] = `<span style='color:cyan;'>${$val['name']}</span>`;
                                        $val['name'] = $val['name'].replace(/\(p\)/,'');
                                    }
                                    $.each($val, function($k,$v) {
                                        e = e.replace(new RegExp(`{{${$k}}}`, 'g'),$v);
                                        //console.log($k);
                                        tmp[$key][$k] = $v;
                                    });
                                    // Remplacement spécial.
                                    //e = e.replace(new RegExp('{{name}}','g'),tmp[$key]['name']);
                                    Stockage.InitDataEntry(tmp);
                                    e = e.replace('\\"','');
                                    //console.log(e);
                                    
                                    //console.log(e);
                                    $("div[id=data]").append( e );
                                    $("app-page").html( this.base );

                                    Stockage.IncrementJsonLength();
                                });

                                // -console.log($("div[id=data]").html());

                            });
                        } else if(this.page == "movies") {
                            Stockage.ResetDataEntry();
                            Stockage.ResetJsonLength();
                            // -console.log(Stockage.GetDataEntry());
                            let tmp = new Object();
                            $.getJSON( this.filename, data => {
                                $.each( data, function($key,$val) {
                                    var e = $("app-page").html();
                                    e = e.replace('{{id}}',$key);
                                    tmp[$key] = new Object();
                                    tmp[$key]['id'] = $key;

                                    // Traitement spécial pour l'urlstream
                                    // Remplace {{name}} par le nom de la série dans {{urlstream}}.
                                    $val['urlstream'] = $val['urlstream'].replace(new RegExp('{{name}}','g'),$val['name']);
                                    // Traitement spécial pour name
                                    // Définie le film comme vue.
                                    let checkStateView = $val['name'].match(/\(v\)/);

                                    if(checkStateView != null) {
                                        $val['name'] = $val['name'].replace(/\(v\)/,'');
                                        // On ajoute la clé état dynamiquement.
                                        $val['etat'] = "Vue";
                                    } else {
                                        $val['etat'] = "Pas encore vue";
                                    }
                                    
                                    $.each($val, function($k,$v) {
                                        e = e.replace(new RegExp(`{{${$k}}}`, 'g'),$v);
                                        // -console.log($k);
                                        tmp[$key][$k] = $v;
                                    });

                                    Stockage.InitDataEntry(tmp);
                                    e = e.replace('\\"','');
                                    
                                    $("div[id=data]").append( e );
                                    $("app-page").html( this.base );

                                    Stockage.IncrementJsonLength();
                                });
                            });
                        } else if(this.page == "mangas") {
                            Stockage.ResetDataEntry();
                            Stockage.ResetJsonLength();
                            // -console.log(Stockage.GetDataEntry());
                            let tmp = new Object();
                            $.getJSON( this.filename, data => {
                                $.each( data, function($key,$val) {
                                    var e = $("app-page").html();
                                    e = e.replace('{{id}}',$key);
                                    tmp[$key] = new Object();
                                    tmp[$key]['id'] = $key;
                                        
                                    // Traitement spécial pour l'urlstream
                                    // Remplace {{name}} par le nom de la série dans {{urlstream}}.
                                    $val['urlstream'] = $val['urlstream'].replace(new RegExp('{{name}}','g'),$val['name']);
                                    // Traitement spécial pour name
                                    // Ajoute la couleur rouge foncé quand la série est défini comme "terminée".
                                    let checkStateTerminee = $val['name'].match(/\(f\)/);
                                    // Ajoute la couleur violet quand la série est défini comme "annulée".
                                    let checkStateAnnulee = $val['name'].match(/\(c\)/);
                                    // Ajoute la couleur verte quand la série est défini comme "prod" et supprime le message "prod".
                                    let checkStateProd = $val['name'].match(/\(p\)/);
                                    // Ajoute la couleur orange quand la série est prévue pour être acheter.
                                    let checkStateBuy = $val['name'].match(/\(a\)/);

                                    if(checkStateTerminee != null) {
                                        let val = $val;
                                        $val['name'] = `<span style='color: darkred;'>${$val['name']}</span>`;
                                        $val['name'] = $val['name'].replace(/\(f\)/,'');
                                    } else if(checkStateAnnulee != null) {
                                        $val['name'] = `<span style='color:red;'>${$val['name']}</span>`;
                                        $val['name'] = $val['name'].replace(/\(c\)/,'');
                                    } else if(checkStateProd != null) {
                                        $val['name'] = `<span style='color:cyan;'>${$val['name']}</span>`;
                                        $val['name'] = $val['name'].replace(/\(p\)/,'');
                                    } else if(checkStateBuy != null) {
                                        $val['name'] = `<span style='color:orange;'>${$val['name']}</span>`;
                                        $val['name'] = $val['name'].replace(/\(a\)/,'');
                                    }

                                    $.each($val, function($k,$v) {
                                        e = e.replace(new RegExp(`{{${$k}}}`, 'g'),$v);
                                        //console.log($k);
                                        tmp[$key][$k] = $v;
                                    });

                                    // Remplacement spécial.
                                    //e = e.replace(new RegExp('{{name}}','g'),tmp[$key]['name']);
                                    Stockage.InitDataEntry(tmp);
                                    e = e.replace('\\"','');
                                    //console.log(e);
                                    
                                    //console.log(e);
                                    $("div[id=data]").append( e );
                                    $("app-page").html( this.base );

                                    Stockage.IncrementJsonLength();
                                });

                                // -console.log($("div[id=data]").html());

                            });
                        } else if(this.page == "books") {
                            Stockage.ResetDataEntry();
                            Stockage.ResetJsonLength();
                            // -console.log(Stockage.GetDataEntry());
                            let tmp = new Object();
                            $.getJSON( this.filename, data => {
                                $.each( data, function($key,$val) {
                                    var e = $("app-page").html();
                                    e = e.replace('{{id}}',$key);
                                    tmp[$key] = new Object();
                                    tmp[$key]['id'] = $key;
                                        
                                    // Traitement spécial pour l'urlstream
                                    // Remplace {{name}} par le nom de la série dans {{urlstream}}.
                                    $val['urlstream'] = $val['urlstream'].replace(new RegExp('{{name}}','g'),$val['name']);
                                    // Traitement spécial pour name
                                    // Ajoute la couleur rouge foncé quand la série est défini comme "terminée".
                                    let checkStateTerminee = $val['name'].match(/\(f\)/);
                                    // Ajoute la couleur violet quand la série est défini comme "annulée".
                                    let checkStateAnnulee = $val['name'].match(/\(c\)/);
                                    // Ajoute la couleur verte quand la série est défini comme "prod" et supprime le message "prod".
                                    let checkStateProd = $val['name'].match(/\(p\)/);
                                    let checkStateBuy = $val['name'].match(/\(a\)/);

                                    if(checkStateTerminee != null) {
                                        let val = $val;
                                        $val['name'] = `<span style='color: darkred;'>${$val['name']}</span>`;
                                        $val['name'] = $val['name'].replace(/\(f\)/,'');
                                    } else if(checkStateAnnulee != null) {
                                        $val['name'] = `<span style='color:red;'>${$val['name']}</span>`;
                                        $val['name'] = $val['name'].replace(/\(c\)/,'');
                                    } else if(checkStateProd != null) {
                                        $val['name'] = `<span style='color:cyan;'>${$val['name']}</span>`;
                                        $val['name'] = $val['name'].replace(/\(p\)/,'');
                                    } else if(checkStateBuy != null) {
                                        $val['name'] = `<span style='color:orange;'>${$val['name']}</span>`;
                                        $val['name'] = $val['name'].replace(/\(a\)/,'');
                                    }

                                    $.each($val, function($k,$v) {
                                        e = e.replace(new RegExp(`{{${$k}}}`, 'g'),$v);
                                        //console.log($k);
                                        tmp[$key][$k] = $v;
                                    });

                                    // Remplacement spécial.
                                    //e = e.replace(new RegExp('{{name}}','g'),tmp[$key]['name']);
                                    Stockage.InitDataEntry(tmp);
                                    e = e.replace('\\"','');
                                    //console.log(e);
                                    
                                    //console.log(e);
                                    $("div[id=data]").append( e );
                                    $("app-page").html( this.base );

                                    Stockage.IncrementJsonLength();
                                });

                                // -console.log($("div[id=data]").html());

                            });
                        }
                    }
                }

                var SeasonManager = SeasonManager || {};

                // ----- SEASON MANAGER ------

                SeasonManager = {

                    refDb: "",
                    namePage: "home",
                    appPage: "",
                    saveIdEdit: 0,
                    saveIdDelete: 0,
                    type: "add",

                    _ParseDataJSON() {
                        this.refDb.GetFileJSON();
                    },

                    _ActivateEventEnter() {
                        if(this.namePage != "home") {
                            // -console.log($( "input[name=memo]" ).attr('id'));
                            //let $this = this;
                            $( "input[name=memo]" ).keydown(event => {
                                if ( event.which == 13 ) {
                                    if(this.type == "add") { this.AddNewEntry(); }
                                    else if(this.type == "edit") { this.EditEntry(); }
                                    event.preventDefault();
                                }
                            });
                        }
                    },

                    _ActivateNag() {
                        $('.cookie.nag').nag('show');
                    },

                    _InitPage($page) {
                        this.namePage = $($page).data('name');
                        $($page).addClass('active');

                        this.refDb = new Database(this.namePage);
                        let $this = this;

                        $("div[data-id=content]").html("<div class='ui segment'><div class='ui active loader'></div></div>");

                        $("div[data-id=content]").load(`__webTpl/${this.namePage}.html`, () => {
                            $("app-page").html("<div class='ui segment'><div class='ui active loader'></div></div>");
                            $("app-page").load(`__webTpl/${this.namePage}.tpl`, function() {
                                $(this).css('display','none');
                                $this.appPage = $("app-page").html();
                                $this._ParseDataJSON();
                                $this._ActivateEventEnter();
                                $this._ActivateNag();
                            });
                        });
                    },

                    _ApplyPage() {
                        this._ResetPage();
                        this.refDb = new Database(this.namePage);
                        $("app-page").load(`__webTpl/${this.namePage}.tpl`, e => {
                            $(e.currentTarget).css('display','none');
                            this.appPage = $("app-page").html();
                            this._ParseDataJSON();
                            $("div[id=data]").removeClass('active loader');
                        });
                    },

                    _ResetPage() {
                        $("div[id=data]")
                            .html('')
                            .addClass('active loader');
                    },

                    NavigateMenu() {
                        $(".menu .item").on('click',e => {
                            $(e.currentTarget).parent().find(`a[data-name=${this.namePage}]`).removeClass('active');
                            this._InitPage(e.currentTarget);
                        });
                    },

                    OpenUINewEntry() {
                        // # Correction d'un bug quand on navigue entre deux fenêtre certaine affiche 2 fenetres.
                        $modal = $(`.ui.modal.${this.namePage}`);
                        if($($modal).length > 1) {
                            $(`${$modal}:last`).remove();
                        }

                        $($modal).modal('show')
                            .find('.header')
                            .html(`Ajouter (${this.namePage})`);

                        // On change le bouton.
                        $($modal).find(".positive.button")
                            .attr('onclick','SeasonManager.AddNewEntry()')
                            .html('Ajouter <i class="checkmark icon"></i>');
                        
                        // On reset tout.
                        let data = Stockage.GetDataEntry();
                        let rootModal = $($modal).find('.ui.form');

                        this._GetType(this.namePage).forEach(e => {
                            $(rootModal).find(`input[name=${e}]`).val('');
                        });

                        this.type = "add";
                    },

                    OpenUIEditEntry($id) {
                        // # Correction d'un bug quand on navigue entre deux fenêtre certaine affiche 2 fenetres.
                        if($(`.ui.modal.${this.namePage}`).length > 1) {
                            $(`.ui.modal.${this.namePage}:last`).remove();
                        }

                        $(`.ui.modal.${this.namePage}`).modal('show');
                        $(`.ui.modal.${this.namePage}`).find('.header').html('Edition (' + this.namePage + ')');

                        // On change le bouton.
                        $(`.ui.modal.${this.namePage}`).find(".positive.button").attr('onclick','SeasonManager.EditEntry()');
                        $(`.ui.modal.${this.namePage}`).find(".positive.button").html("Sauvegarder " + '<i class="checkmark icon"></i>');

                        // On charge les infos.
                        let data = Stockage.GetDataEntry();
                        let rootModal = $(`.ui.modal.${this.namePage}`).find('.ui.form');

                        this._GetType(this.namePage).forEach(e => {
                             $(rootModal).find(`input[name=${e}]`).val(data[$id][e]);
                        });

                        // On retire les balises <span> dans le nom quand on veut éditer.
                        if(this.namePage == "series") {
                            let check = data[$id]['name'].match(/<span style='(.+)'>(.+)<\/span>/);
                            if(check != null) {
                                data[$id]['name'] = data[$id]['name'].replace(/<span style='(.+)'>(.+)<\/span>/,'$2');
                            }
                        }

                        this.saveIdEdit = $id;
                        this.type = "edit";
                    },

                    OpenUIDeleteEntry($id) {
                        // # Correction d'un bug quand on navigue entre deux fenêtre certaine affiche 2 fenetres.
                        if($(".ui.confirmDelete").length > 1) {
                            $(".ui.confirmDelete:last").remove();
                        }
                        $(".ui.confirmDelete").modal('show');
                        this.saveIdDelete = $id;
                    },

                    AddNewEntry() {
                        let id = Stockage.GetJsonLength();
                        let tmp = new Object();
                        let rootModalSerie = $(`.ui.modal.${this.namePage}`).find('.ui.form');
                        tmp['id'] = id;
                        this._GetType(this.namePage).forEach(e => {
                            let data = $(rootModalSerie).find(`input[name=${e}]`).val();
                            tmp[e] = data;
                        });
                        tmp['lastedit'] = Stockage._GetDate();

                        Stockage.AddDataEntry(tmp);
                        Stockage.SaveDataEntry();

                        this._ApplyPage();
                        $(`.ui.modal.${this.namePage}`).modal('hide');
                    },

                    EditEntry() {
                        let id = this.saveIdEdit;
                        let tmp = new Object();
                        let rootModalSerie = $(`.ui.modal.${this.namePage}`).find('.ui.form');
                        tmp['id'] = id;
                        this._GetType(this.namePage).forEach(e => {
                            let data = $(rootModalSerie).find(`input[name=${e}]`).val();
                            tmp[e] = data;
                        });
                        let editData = Stockage.EditDataEntry(id,tmp);
                        Stockage.SaveDataEntry(editData);
                        this._ApplyPage(this.namePage);
                        // -console.log("Modifier avec succès !");
                        $(`.ui.modal.${this.namePage}`).modal('hide');
                    },

                    DeleteEntry() {
                        let id = this.saveIdDelete;
                        Stockage.RemoveDataEntry(id);
                        Stockage.SaveDataEntry();
                        this._ApplyPage();
                        $(".ui.confirmDelete").modal('hide');
                    },

                    ResetEntry() {
                        let rootModal = $(`.ui.modal.${this.namePage}`).find('.ui.form');
                        this._GetType(this.namePage).forEach(e => {
                            $(rootModal).find(`input[name=${e}]`).val('');
                        });
                        $(`.ui.modal.${this.namePage}`).modal('hide');
                    },

                    CancelEntry() {
                        $(`.ui.modal.${this.namePage}`).modal('hide');
                    },

                    _GetType($page) {
                        switch($page) {
                            case "series":
                            return ["name","urlpicture","urlstream","season","eps","memo"];
                            break;
                            case "movies":
                            return ["name","urlpicture","urlstream","memo"];
                            break;
                            case "mangas":
                            return ["name","urlpicture","urlstream","season","eps","memo"];
                            break;
                            case "books":
                            return ["name","urlpicture","urlstream","page","lastnumber","dispo","memo"];
                            break;
                        }
                    },

                    _GetTypeFull($page) {
                        switch($page) {
                            case "series": 
                            return ["id","name","urlpicture","urlstream","season","eps","memo","lastedit"];
                            break;
                            case "movies": 
                            return ["id","name","urlpicture","urlstream","memo","lastedit"];
                            break;
                            case "mangas": 
                            return ["id","name","urlpicture","urlstream","season","eps","memo","lastedit"];
                            break;
                            case "books":
                            return ["id","name","urlpicture","urlstream","page","lastnumber","dispo","memo","lastedit"];
                            break;
                        }
                    }
                };

                // On active le click pour naviguer dans le menu
                SeasonManager.NavigateMenu();
        </script>
    </body>
</html>