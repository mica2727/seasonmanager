<div class='card' id='container' data-type='id' data-value='{{id}}'>
    <div class="image">
        <img src="{{urlpicture}}" style="height: 380px; width: 370px;">
    </div>
    <div class="content">
        <div class="header">{{name}}</div>
        <div class="description">{{memo}}</div>
    </div>
    <div class="extra content">
        <span class="right floated">{{lastedit}} </span>
        <span><i class="info circle icon"></i>Saison {{season}} Episode {{eps}}</span>
    </div>
    <div class="ui middle attached button"><i class="info icon"></i> <a target="_blank" href="http://animedigitalnetwork.fr/video#search={{name}};">Recherche sur Anime Digital Network</a> </div>
    <div class="ui middle attached button"><i class="info icon"></i> <a target="_blank" href="http://www.qwant.fr/?q={{name}}">Recherche sur QWant</a> </div>
    <div class="ui middle attached button"><i class="info icon"></i> <a target="_blank" href="{{urlstream}}">Regarder un épisode</a> </div>
    <div class="ui middle attached button green" onclick='SeasonManager.OpenUIEditEntry($(this).parent().data("value"))'><i class="write icon"></i> Modifier </div>
    <div class="ui bottom attached button red" onclick='SeasonManager.OpenUIDeleteEntry($(this).parent().data("value"))'><i class="delete icon"></i> Supprimer </div>
</div>