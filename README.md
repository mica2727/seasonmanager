SeasonManager écrit ici comme "Cet outil" est un outil destiné pour ceux qui regarde des séries, des films, des mangas, des livres, et qui en regarde ou lis beaucoup, cet outil est développer par Chevallier Michael.
Je ne suis pas responsable de l'utilisation que vous ferez et comment vous l'utilisez.

#########
   LICENSE  
#########

# SeasonManager
 > Cet outil est destiné à une utilisation personnel, et non commercial.
 > Vous ne pouvez pas vous appropriez mon travail, ni m�me redistribuer cet outil.
 > Si vous avez téléchargé ce programme autre que sur le site officiel (cc-i.ovh) veuillez m'en faire part dans le formulaire de contact du site.
 > Si vous avez payé pour obtenir cet outil s'est une contrefa�on, cet outil est distribué par ProjetsTCF gratuitement. Veuillez m'en faire part.

# Les programmes utilisés
Ce programme est développer avec les outils suivant :
Il y a aucun lien entre mon programme et les outils suivant, je les cite par respect et parce que je les utilise dans mon programme.

 > Un serveur et une plateforme : PHP 7.2.2 (https://php.net).
 > Un framework CSS : Semantic UI (https://semantic-ui.com)
 > Un framework JS : jQuery (https://jquery.com)

 "This product includes PHP software, freely available from
     <http://www.php.net/software/>".